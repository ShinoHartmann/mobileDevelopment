/*Header html will be inserted to each page*/
var headerHtml = '<!--Header--> \
  <div class="header"> \
    <div class="header-role" data-role="header" data-position="inline"> \
      <a href="#{{page}}-menu-panel" class="open left" data-icon="bars" data-iconpos="notext">Menu</a> \
      <h1>HSENS</h1> \
      <a href="#index" class="right" data-icon="home" data-iconpos="notext">Home</a> \
    </div> \
    \
    <!--Panel markup--> \
    <div class="panel left" data-role="panel" data-display="overlay" data-position="left" id="{{page}}-menu-panel"> \
      <ul> \
        <li><a href="#index" data-transition="fade" data-rel="close">Home</a></li> \
        <li><a href="#gallery" class="{{galleryActiveClass}}" data-transition="slide" data-rel="close">Gallery</a></li> \
        <li><a href="#news" class="{{newsActiveClass}}" data-transition="slide" data-rel="close">News</a></li> \
        <li><a href="#quiz" class="{{quizActiveClass}}" data-transition="slide" data-rel="close">Quiz</a></li> \
        <li><a href="#donationForm" class="{{donationFormActiveClass}}" data-transition="slide" data-rel="close">Donation Form</a></li> \
        <li><a href="#contactUs" class="{{contactUsActiveClass}}" data-transition="slide" data-rel="close">Contact Us</a></li> \
      </ul> \
    </div> \
  </div>';

/*Inject headerHtml to each page, assign different ids to the each menu panel, and activate the current page link.*/
function loadHeader() {
  var template = Handlebars.compile(headerHtml);
  $("#index-header").append(template({
    page: "index"
  }));
  $("#gallery-header").append(template({
    page: "gallery",
    galleryActiveClass: "menu-active"
  }));
  $("#news-header").append(template({
    page: "news",
    newsActiveClass: "menu-active"
  }));
  $("#quiz-header").append(template({
    page: "quiz",
    quizActiveClass: "menu-active"
  }));
  $("#donationForm-header").append(template({
    page: "donationForm",
    donationFormActiveClass: "menu-active"
  }));
  $("#contactUs-header").append(template({
    page: "contactUs",
    contactUsActiveClass: "menu-active"
  }));
}

/*It enables the application to open and close by swiping the page.*/
function swideOpenAndCloseAllPages() {
  swipeOpenAndClose("index");
  swipeOpenAndClose("gallery");
  swipeOpenAndClose("news");
  swipeOpenAndClose("quiz");
  swipeOpenAndClose("donationForm");
  swipeOpenAndClose("contactUs");
}

function swipeOpenAndClose(page) {
  $(document).on("pageinit", "#" + page, function () {
    $(document).on("swipeleft swiperight", "#" + page, function (e) {
      if (e.type === "swipeleft") {
        $("#" + page + "-menu-panel").panel("close");
      } else if (e.type === "swiperight") {
        $("#" + page + "-menu-panel").panel("open");
      }
    });
  });
}
