/*this function is used to populate text in different pages using json, as i am using dummy text*/
function injectHtml(templateName, idName, contents) {
  var source = $("#" + templateName).html();
  var template = Handlebars.compile(source);
  var html = template(contents);
  $('#' + idName).append(html);
}

/*this is a popup function to display a confirmation message for the donation*/
function openPopup(popupName){
  $("#" + popupName).popup('open');
}

function validateDonationForm() {
/*As submit button does not exits in the form, the validation occurs when a user clicks "Donate" button*/
  if ($("#donationForm input").jqBootstrapValidation("hasErrors")){
    openPopup("donationError");
  } else {
    openPopup("thankYouForDonation");
    clearForm();
  }
}

/**/
function clearForm(){
  $("form").trigger("reset");
}

function validateContactForm() {
  /*As submit button does not exits in the form, the validation occurs when a user clicks "Send your message" button*/
  if ($("#contactUs input, textarea").jqBootstrapValidation("hasErrors")){
    openPopup("contactError");
  } else {
    openPopup("thankYouForYourMessage");
    clearForm();
  }
}

function loadJqueryMobile() {
  $(document).bind('mobileinit', function () {
    $.mobile.changePage.defaults.changeHash = false;
    $.mobile.hashListeningEnabled = false;
    $.mobile.pushStateEnabled = false;
    $.mobile.defaultPageTransition = 'none';
  });
}

function initGallery() {
  injectHtml("gallery-template", "gallery-images", images);
  /*SimpleLightbox feature is added and the picture is able to popup when a user clicks it and a user is able to view previous or next pictures by swiping the picture.*/
  $('.gallery a').simpleLightbox({
    navText: ['&lsaquo;', '&rsaquo;']
  })
}

function initDonationForm() {
  $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
}

function showPages() {
  $(document).on("pageinit", "#contents", function (event) {
    $("#contents").removeClass("hidden");
  });
}

function initQuizPage() {
  showQuestion(questionNumber++);
}

function initNewsPage() {
  injectHtml("news-template", "news-contents", newsContents);
}

function initPages() {
  loadJqueryMobile();
  loadHeader();
  initGallery();
  initQuizPage();
  initNewsPage();
  initDonationForm();
  loadFooter();
  swideOpenAndCloseAllPages();
  showPages();
}

$(document).ready(function () {
  // are we running in native app or in a browser?
  window.isphone = false;
  if (document.URL.indexOf('http://') === -1 && document.URL.indexOf('https://') === -1) {
    window.isphone = true;
  }

  if (window.isphone) {
    document.addEventListener('deviceready', onDeviceReady, false);
  } else {
    onDeviceReady();
  }
});

function onDeviceReady() {
  /*Splash screen is shown until the application is read completely*/
  if (navigator.splashscreen) {
    navigator.splashscreen.hide();
  }
}
