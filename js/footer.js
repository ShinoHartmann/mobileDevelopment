var footerHtml = ' \
    <!-- Site footer --> \
    <footer data-role="footer" data-position="bottom">\
      <div class="footer_socials clearfix">\
          <a class="footer-link" href="http://www.facebook.com" target="_blank"><img alt="Greendale facebook page" src="images/footer-icons/facebook.png" /></a> \
          <a class="footer-link" href="https://twitter.com" target="_blank"><img alt="Greendale twitter page" src="images/footer-icons/twitter.png" /></a> \
          <a class="footer-link" href="http://pinterest.com" target="_blank"><img alt="Greendale pinterest page" src="images/footer-icons/pinterest.png" /></a> \
          <a class="footer-link" href="https://plus.google.com" target="_blank"><img alt="Greendale google plus page" src="images/footer-icons/google-plus.png" /></a> \
          <a class="footer-link" href="http://www.youtube.com" target="_blank"><img alt="Greendale youtube page" src="images/footer-icons/youtube.png" /></a> \
      </div> \
      <p class="copyright">Copyright &copy; Shino Hartmann</p> \
    </footer>';


function loadFooter() {
  $(".footer").append(footerHtml);
}
