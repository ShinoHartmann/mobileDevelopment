/*To load quiz*/
var questionTemplate;
var questionNumber = 0;
var score = 0;

function loadQuestionTemplate() {
  var source = $("#quiz-template").html();
  questionTemplate = Handlebars.compile(source);
}

/*
This function inject a quiz and answers to HTML of the quiz page.
*/
function showQuestion(number) {
  if (!questionTemplate) {
    loadQuestionTemplate();
  }
  var html = questionTemplate(questions[number]);
  var hidden = $(html).hide();
  /*Remoce prevous quiz from HTML*/
  $('#quiz-contents').empty();
  /*Add next quiz to HTML*/
  $('#quiz-contents').append(hidden);
  showScore();
  if (questions.length == (number + 1)) {
    // if this question is the last question, show final score
    $("#finalResult").removeClass("hidden");
    questionNumber = 0;
  } else {
    $("#nextQuestion").removeClass("hidden");
  }
  hidden.slideDown(100);
}

function showAnswer(selectedVal, answer) {
  $("#result").removeClass("hidden");
  if (selectedVal == answer) {
    score += 10;
    showScore();
    message = "You are correct!";
    $("#correctUserAnswer").addClass("rightAnswer");
  } else {
    message = "You are wrong... The right answer is: " + answer;
    $("#correctUserAnswer").addClass("wrongAnswer");
  }
  $("#correctUserAnswer").append(message);
}

var checkAnswser = function () {
  var selected = $("input[type='radio'][name='choice']:checked");
  var selectedValue = 0;
  var answer = $("#answer").text();
  if (selected.length > 0) {
    selectedValue = parseInt(selected.val()) + 1;
    $("#show-answer-button").addClass("hidden");
    showAnswer(selectedValue, answer);
  } else {
    openPopup("errorMessage");
  }
}

var showScore = function () {
  $("#score").empty();
  $("#score").append("Score: " + score);
}

var goToNextQuestion = function () {
  showQuestion(questionNumber++);
  $("#quiz-contents").enhanceWithin();
}

var showFinalScore = function() {
  $("#showFinalScore p").html("Your final score is: " + score);
  //open popup
  score = 0;
  openPopup("showFinalScore");
}
