var newsContents = {
  newsContents: [
    {
      name: "news1",
      title: "MDR TB in Pakistan and Myanmar",
      image_src: "images/1.jpg",
      date: "Monday, August 3, 2015",
      summary: "Studies on multi-drug resistant tuberculosis in Pakistan and Myanmar.",
      details: "Mishal Khan is a Lecturer in Public Health in the department of Global Health & Development and an Assistant Professor at the National University of Singapore. She is experienced in epidemiological and operational research and policy analysis. Mishal’s main research areas include gender inequalities, health systems strengthening, public-private partnerships and tuberculosis control. She is currently based in Singapore, leading studies in South and South-East Asian low income countries including Pakistan, China, Myanmar and Cambodia. Her interest is in developing locally appropriate, sustainable interventions and policy measures to improve health."
},
    {
      name: "news2",
      title: "One Health: parasites and beyond…",
      image_src: "images/2.jpg",
      date: "Monday, September 14, 2015",
      summary: "Autumn Symposium of the British Society for Parasitology on ‘One Health: parasites and beyond…’ in association with the British Society for Parasitology, MSD Animal Health and Ceva Animal Health Ltd.",
      details: "The Royal Veterinary College, London School of Hygiene & Tropical Medicine and London International Development Centre present an Autumn Symposium on ‘One Health: parasites and beyond…’ in association with the British Society for Parasitology, MSD Animal Health and Ceva Animal Health Ltd. The One Health approach promotes collaboration between multiple disciplines including biology, human and veterinary medicine, environmental sciences, economics and social sciences in order to improve human, animal and environmental health. In this symposium we will bring together experts in different fields to explore how an interdisciplinary approach can improve control of parasitic diseases of humans and animals."
},
    {
      name: "news3",
      title: "Malcolm Potts - Crisis in the Sahel",
      image_src: "images/3.jpg",
      date: "Tuesday, July 21, 2015",
      summary: "Professor Malcolm Potts, renowned in the field of public health, gave a one-off keynote lecture for LIDC. The lecture, at the London School of Hygiene and Tropical Medicine, focused on the emerging humanitarian disaster in the Sahel. It has been central to the work of his team for several years. ",
      details: "Professor Malcolm Potts, renowned in the field of public health, gave a one-off keynote lecture for LIDC. The lecture, at the London School of Hygiene and Tropical Medicine, focused on the emerging humanitarian disaster in the Sahel. It has been central to the work of his team for several years. There are humanitarian, food security, ecological refugee migration, and conflict terrorism implications of this crisis. In the middle of the 21st century it will involve more people than live in the USA. There are also evidence-based measures to ameliorate the crisis, including agricultural adaptations, improving access to voluntary family planning, building human capital and efforts to to keep girls in secondary school. Potts argues that one of the most effective ways to mitigate disaster is to give people access to family planning. Even merely meeting the need for everyone that wants family planning and contraceptive advice to have access now, would be enough to make a huge impact on population numbers in the future. Let alone promoting policies that reduce the fertility rate."
}
  ]};
