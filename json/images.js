var images = {
  images: [
    {"name": "image1.jpg", "alt": "Image 1", "title":"Image 1"},
    {"name": "image2.jpg", "alt": "Image 2", "title":"Image 2"},
    {"name": "image3.jpg", "alt": "Image 3", "title":"Image 3"},
    {"name": "image4.jpg", "alt": "Image 4", "title":"Image 4"},
    {"name": "image5.jpg", "alt": "Image 5", "title":"Image 5"},
    {"name": "image6.jpg", "alt": "Image 6", "title":"Image 6"},
    {"name": "image7.jpg", "alt": "Image 7", "title":"Image 7"},
    {"name": "image8.jpg", "alt": "Image 8", "title":"Image 8"},
    {"name": "image9.jpg", "alt": "Image 9", "title":"Image 9"},
    {"name": "image10.jpg", "alt": "Image 10", "title":"Image 10"},
    {"name": "image11.jpg", "alt": "Image 11", "title":"Image 11"},
    {"name": "image12.jpg", "alt": "Image 12", "title":"Image 12"}
  ]
};
