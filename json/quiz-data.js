var questions = [
  {
    "question": "Q1. 3 * 5 =?",
    "choices": [
      "1. 8",
      "2. 10",
      "3. 15",
      "4. 20"
      ],
    "answer": "3",
    "explanation": "Three times five equals fifteen"
  },
  {
    "question": "Q2. Toronto is the capital city of",
    "choices": [
      "1. Ontario",
      "2. Canada",
      "3. Ottawa",
      "4. None of the above"
      ],
    "answer": "1",
    "explanation": "Toronto is the largest city in Canada population-wise. It's also the capital of Ontario, which is a province. The political and official capital of Canada is Ottawa."
  },
  {
    "question": "Q3. Which country was not allied to Germany during WWI?",
    "choices": [
      "1. Romania",
      "2. Bulgaria",
      "3. The Ottoman Empire",
      "4. Austria-Hungary"
      ],
    "answer": "1",
    "explanation": "Romania was allied to Germany during WWII not WWI."
  },
  {
    "question": "Q4. 15 / 3 =?",
    "choices": [
      "1. 5",
      "2. 45",
      "3. 18",
      "4. Ao"
      ],
    "answer": "1",
    "explanation": "15 / 3 = 5"
  },
  {
    "question": "Q5. 2*(3+5)=?",
    "choices": [
      "1. 11",
      "2. 13",
      "3. 16",
      "4. 30"
      ],
    "answer": "3",
    "explanation": "You should calculate (3+5) at first, then multiply 2"
  },
  {
    "question": "Q6. What is the capital of Japan?",
    "choices": [
      "1. Kyoto",
      "2. Hongkong",
      "3. London",
      "4. Tokyo"
      ],
    "answer": "4",
    "explanation": "Hongkong is a city of China, London is a city of UK. Kyoto is a city of Japan, but not the capital of Japan."
  },
  {
    "question": "Q7. what is the capital of UK?",
    "choices": [
      "1. London",
      "2. Paris",
      "3. Kathmandu",
      "4. Tokyo"
      ],
    "answer": "3",
    "explanation": "Paris is a capital of France, Kathmanduis a capital of Nepal and Tokyo is a capital of Japan."
  },
  {
    "question": "Q8. Who invent facebook?",
    "choices": [
      "1. Sabeer Bhatia",
      "2. Mark Zuckerberg",
      "3. Jack Dorsey",
      "4. Steve Jobs"
      ],
    "answer": "2",
    "explanation": "Sabeer Bhatia is a founder of Hotmail.com, Jack Dorsey is a CEO of Twitter and Steve Jobs was CEO of Apple Inc."
  },
  {
    "question": "Q9.What is the population of the uk?",
    "choices": [
      "1. 100 million",
      "2. 64 million",
      "3. 7 billion",
      "4. 6.4 million"
      ],
    "answer": "2",
    "explanation": "The population of the UK as of 2013 was 64.1 million."
  },
  {
    "question": "Q10. When does WW2 happened?",
    "choices": [
      "1. 1939",
      "2. 1945",
      "3. 1949",
      "4. 1950"
      ],
    "answer": "1",
    "explanation": "World War 2 started from 1939 to 1945."
  }
];
